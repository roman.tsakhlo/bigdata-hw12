from confluent_kafka import Consumer, Producer
from datetime import datetime

kafka_bootstrap_servers = 'localhost:9092'
input_topic = 'input_stream'
output_topic = 'output_stream'

consumer_config = {
    'bootstrap.servers': kafka_bootstrap_servers,
    'group.id': 'message_processor',
    'auto.offset.reset': 'earliest'
}
consumer = Consumer(consumer_config)
consumer.subscribe([input_topic])

producer_config = {'bootstrap.servers': kafka_bootstrap_servers}
producer = Producer(producer_config)

def process_message(msg):
    original_message = msg.value().decode('utf-8')
    message_length = len(original_message)
    processing_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

    processed_message = f"Message_length: {message_length}; Time of processing: {processing_time}; Original message: {original_message}"

    producer.produce(output_topic, key=msg.key(), value=processed_message.encode('utf-8'))
    producer.flush()

def main():
    try:
        while True:
            msg = consumer.poll(1.0)

            if msg is None:
                continue
            if msg.error():
                print(f"Error message: {msg.error()}")
                continue

            process_message(msg)

    except KeyboardInterrupt:
        pass
    finally:
        consumer.close()

if __name__ == '__main__':
    main()
